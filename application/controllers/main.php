<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller
{

    public function index()
    {
        date_default_timezone_set('Asia/Dubai');
        $this->load->library('table');
        $this->load->helper(array('url', 'form', 'date'));
        $this->load->library('form_validation');
        $this->load->model('model_mysql_class'); //exit();
        $this->load->view('main_view');
    }

    public function display()
    {
        date_default_timezone_set('Asia/Dubai');
        $this->load->library('table');
        $this->load->model('model_mysql_class'); //exit();
        $this->load->model('short_url_model');
        $this->short_url_model->display();
        $this->load->view('processed_url/processed_url');
    }

    public function hide_user_url()
    {
        date_default_timezone_set('Asia/Dubai');
        $this->load->library('table');
        $this->load->model('model_mysql_class'); //exit();
        $this->load->model('short_url_model');
        $this->short_url_model->hide_user_url();
        $this->load->view('hide_user_url/hide_user_url');
    }
    
  
     public function hide_user_url_register()
    {
        date_default_timezone_set('Asia/Dubai');
        $this->load->library('table');
        $this->load->model('model_mysql_class'); //exit();
        $this->load->model('short_url_model');
        $this->short_url_model->hide_user_url_register();
        $this->load->view('Lib/AJAX/AJAX_empty_row');
    }   
    

}
