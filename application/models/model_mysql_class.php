<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_mysql_class extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

    public function randStrGen($length, $charset = 'abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    {
        $Str = '';
        $j = 0;
        $CharCount = strlen($charset);

        for ($j = 0; $j < $length; $j++) {
            $Str .= $charset[mt_rand(0, $CharCount - 1)];
        }

        $RandStr = str_shuffle($Str);
        return $RandStr;
    }

    public function StringTrim($s, $lenght)
    {
        $str_to_count = html_entity_decode($s);

        if (strlen($str_to_count) <= $lenght) {
            return $s;
        }

        $s2 = substr($str_to_count, 0, $lenght - 3);
        $s2 .= "...";

        return htmlentities($s2);
    }

    public function white_space_strip($Strn)
    {
        $ProcStr = 0;

        $NAC = array("#", "%", "&", "*", ":", ";", "<", ">", "?", "{", "}", "|", "!");
        $ProcStr = str_replace($NAC, "", str_replace("'", "\'", trim(preg_replace('/[\s\t\n\r\s]+/', ' ', $Strn))));

        return $ProcStr;
    }

    public function htmlentities($Strn)
    {
        return htmlentities($Strn);
    }

    public function mysql_real_escape_string($Strn)
    {
        return mysql_real_escape_string($Strn);
    }

    public function GetIP()
    {
        $ip;
        if (getenv("HTTP_CLIENT_IP"))
            $ip = getenv("HTTP_CLIENT_IP");
        else if (getenv("HTTP_X_FORWARDED_FOR"))
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        else if (getenv("REMOTE_ADDR"))
            $ip = getenv("REMOTE_ADDR");
        else
            $ip = "UNKNOWN";
        return $ip;
    }

    public function UserUrlReturn($ID)
    {
        $query = $this->db->query("select * from tblUserUrl where CohortID ='$ID'");
        $row = $query->row();

        $record = array();

        $record[] = $row->UserURL; //echo  $record[0].'  $record[0]<br />';
        $record[] = $row->UserIP;
        $record[] = $row->rec_time;
        $record[] = $row->shortner;
        $record[] = $row->visible;

        return $record;
    }

}
