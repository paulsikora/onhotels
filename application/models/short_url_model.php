<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Short_url_model extends CI_Model
{

    public function __construct()
    {
        /*
          Magic Method __construct() is used here to initialize a CI database class for use in some of the methods
         * 
         */

        $this->load->database(); // load DB extension
        $this->load->helper('url');
    }

    public function display()
    {
        $this->load->database();

        //convert POST to htmlentities
        $longURL_passed = $this->input->post('LongURL');

        //convert POST to mysql_real_escape_string
        //$longURL_passed = $this->model_mysql_class->mysql_real_escape_string($longURL_passed);
        $user_IP = $this->model_mysql_class->GetIP();
        $rec_time = time();




        $i = 1;
        while ($i == 1) {

            // Create an identifier 15 chrs long
            $shortner = $this->model_mysql_class->randStrGen(5);

            //Check if exist
            $sql = "SELECT * FROM tblUserUrl where  "
                    . "Shortner = '$shortner' ";

            $queryBlock = $this->db->query($sql);
            $num_rows = $queryBlock->num_rows();

            if ($num_rows == 0) {
                $i = 0;
            } else {
                $shortner = $this->model_mysql_class->randStrGen(5);
            }
        }









        $ch = curl_init();

// set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $longURL_passed);
        curl_setopt($ch, CURLOPT_HEADER, false);

// grab URL and pass it to the browser
//curl_exec($ch);
// close cURL resource, and free up system resources
        curl_close($ch);



        /*
          The purpose of this query is to check if the URL in question  has already been
          processed. The ShortURL
         * 
         * ALPHA / DIGIT / "-" / "." / "_" / "~"
         * 
         * 
         * 
         */

        $sql = "SELECT * FROM tblUserUrl where  "
                . "UserURL = '$longURL_passed' ";



        $queryBlock = $this->db->query($sql);
        $num_rows = $queryBlock->num_rows();

        if ($num_rows == 0) {
            $data = array(
                'UserURL' => $longURL_passed,
                'UserIP' => $user_IP,
                'rec_time' => $rec_time,
                'shortner' => $shortner
            );

            $this->db->insert('tblUserUrl', $data);
        }
    }

    public function hide_user_url()
    {
        $this->load->database();
    }

    public function hide_user_url_register()
    {
        $this->load->database();

        $UserURLID_passed = html_entity_decode($this->input->post('UserURLID'));

        $data = array(
            'visible' => 1
        );

        $this->db->where('UserURLID', $UserURLID_passed);
        $this->db->update('tblUserUrl', $data);
    }

}
