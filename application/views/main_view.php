<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if ($this->input->get('su')) {

    $su_Passed = $this->model_mysql_class->htmlentities($this->input->get('su'));
    $su_Passed = $this->model_mysql_class->mysql_real_escape_string($su_Passed);

    $sql = "SELECT * FROM tblUserUrl where  "
            . "Shortner = '$su_Passed' ";

    $query = $this->db->query($sql);
    $row = $query->row();
    $UserURLID = $row->UserURLID;
    $UserURL = $row->UserURL;

    $user_IP = $this->model_mysql_class->GetIP();
    $rec_time = time();

    $data = array(
        'UserURLID_FK' => $UserURLID,
        'HitDate' => $rec_time,
        'HitIP' => $user_IP
    );

    $this->db->insert('tblHits', $data);



    $url = $UserURL;
    redirect($url);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Short Url Service - Paul Sikora</title>

        <script language="JavaScript" type="text/javascript" src="<?php echo base_url(); ?>AjaxFormReturn.js"></script>

        <style type="text/css">
            .BulletBold { font-style:normal; font-weight:bold
            }
        </style>
    </head>

    <body>
        <div id="TopDiv" style="position:absolute; left:0%; top:5%; width:100%; 
             height:12%; z-index:1; border-bottom:solid #DBEBD6 1px;  
             border-top:solid #DBEBD6 1px;  ">

            <div align="left">
                <img src="<?php echo base_url(); ?>imgs1/shorten_URL_service.png" 
                     width="258" height="68" alt="Shorten URL Service" 
                     style="margin-left:30px; margin-top:5px;  " />
            </div>
        </div>

        <div id="ConversionDiv" style="position:absolute; left:1%; top:20%; 
             width:49%; height:70%; z-index:1; background-color:#F9F9F9; ">

            <form name="form1" id="test" onSubmit="return false;"  style="display: inline; margin: 0;" >
                <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="20%"  >
                            <div align="center">
                                <table width="95%" border="0" cellspacing="0" 
                                       cellpadding="0"  style=" background-color:#F5FEF5;  
                                       border-bottom:solid #DBEBD6 1px;  
                                       border-top:solid #DBEBD6 1px; "   >
                                    <tr>
                                        <td colspan="2"><div align="center">&nbsp;</div></td>
                                    </tr>

                                    <tr>
                                        <td width="60%" height="41"><div align="left">
                                                <input name="LongURL" type="text" 
                                                       id="LongURL" style="font:Arial, 
                                                       Helvetica, sans-serif; font-size:14px; 
                                                       font-style:normal; font-weight:normal; 
                                                       width:90%; height:32px; margin-left:10%; padding-left:2px;  " 
                                                       value="Enter Long URL ( 256 characters max )" 
                                                       maxlength="254" onclick='javascript: this.value = ""' />
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div align="center">
                                                <?php
                                                echo '<input type="image" name="imageField" id="imageField" '
                                                . 'src="' . base_url() . 'imgs1/shorten.png" '
                                                . 'onclick="javascript: formget(this.form, \'' . base_url() . 'index.php/main/display\', \'ResultDiv\');  "  '
                                                . 'title="Shorten link"  />';
                                                ?>
                                            </div>


                                        </td>

                                    </tr>


                                    <tr>


                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>

                                    </tr>

                                </table>

                            </div>


                        </td>

                    </tr>

                    <tr>

                        <td height="80%" style="vertical-align:top;" >


                            <div align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-style:normal;">
                                <p>&nbsp;</p>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td><div align="center"><span style="font-family:Arial, Helvetica, sans-serif; font-size:18px; font-style:normal;">Technical Test for On Hotels</span></div></td>
                                    </tr>
                                    <tr>
                                        <td><div align="center"><a href="<?php echo base_url(); ?>repository.zip">Repository </a> &copy; 2015 Paul Sikora  all rights reserved</div></td>
                                    </tr>
                                    <tr>
                                        <td id="info"><div align="left" style="margin:1%;  border: thin 1px solid #CCC; background-color:#F3F3F3; padding:2px;  ">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="89%">&nbsp;</td>
                                                        <td width="11%">Close
                                                            <?php
                                                            echo '<input type="image" name="imageField" id="imageField" '
                                                            . 'src="' . base_url() . 'imgs1/close_ai.png" '
                                                            . 'onclick="javascript: formget(this.form, \'' . base_url() . 'index.php/main_lib/AJAX_empty_row\', \'info\');  "  '
                                                            . 'Close"  />';
                                                            ?>   

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                </table>


                                                <p>Produce a URL shortening service of your own</p>
                                                <p>I undertook this task on Friday with an intention to complete it on 20/04 2015. The project is functioning as requested. The URL uniqeness is not based on the tables row's primary key. Every unique identifier is a random string, and for the purpose of this exercise is exactly 5 characters long, The reason for not using a  primary key as a unique identifier is the fact that PK uses auto-increment. With a large number of records the site purpose would colide with its functionality. <br />
                                                    <br />
                                                    The only extra feature is the ability to hide already processed URLs. </p>
                                                <p>Project Limitations:</p>
                                                <ol>
                                                    <li>As at present the project is designed to take a valid URL which is no longer than 256 characters. From the research conducted a various browsers can have a diferent URLs lenght limitation, but that one should cover them without any problems.</li>
                                                    <li>As there were no requests for any validation, this project development was focused on its primary functionality, that is the URL shortening only. <br />
                                                        <br />
                                                        There is no:<br />
                                                        - exteme data testing<br />
                                                        - test for broken URLs<br />
                                                        - SQL injection<br />
                                                        - validity of submitted data ( lenght and type )
                                                        <br />
                                                        - No pgination
                                                    </li>
                                                </ol>
                                                <p>&nbsp;</p>
                                                <ul>
                                                    <li><span class="BulletBold" >Knowledge of MVC development</span>
                                                        <blockquote>
                                                            <p>Whole test has been done in CodeIgniter MVC framework</p>
                                                        </blockquote>
                                                    </li>

                                                    <li><span class="BulletBold" >Ability to follow PSR standards, specifically 0 to 2</span>
                                                        <blockquote>
                                                            <p>&lt;?php</p>
                                                            <p>if (!defined('BASEPATH'))<br />
                                                                exit('No direct script access allowed');<br />
                                                            </p>
                                                            <p>$UserURLID_passed = html_entity_decode($this-&gt;input-&gt;post('UserURLID'));<br />
                                                                $ID_String_passed = html_entity_decode($this-&gt;input-&gt;post('ID_String'));</p>
                                                            <p>&nbsp;</p>
                                                            <p>$i = 1;<br />
                                                                $GridRowLight = '#F7F7F7';<br />
                                                                $GridRowDark = '#fbfbfe';</p>
                                                            <p>$sql = &quot;select * from tblUserUrl where UserURLID = '$UserURLID_passed' &quot;;<br />
                                                                $queryUserUrl = $this-&gt;db-&gt;query($sql);<br />
                                                                //$rowAssCritUnitAssignment = $queryAssCritUnitAssignment-&gt;row();<br />
                                                                $num_rowUserUrl = $queryUserUrl-&gt;num_rows();</p>
                                                            <p>foreach ($queryUserUrl-&gt;result_array() as $rowUserUrl) {<br />
                                                                $UserURLID = $rowUserUrl['UserURLID'];<br />
                                                                $UserURL = html_entity_decode($rowUserUrl['UserURL']);<br />
                                                                $UserIP = $rowUserUrl['UserIP'];<br />
                                                                $rec_time = $rowUserUrl['rec_time'];<br />
                                                                $shortner = $rowUserUrl['shortner'];<br />
                                                                $visible = $rowUserUrl['visible'];</p>
                                                            <p>&nbsp;</p>
                                                            <p> $sqlHits = &quot;select * from tblHits where UserURLID_FK = '$UserURLID'  &quot;;<br />
                                                                $queryHits = $this-&gt;db-&gt;query($sqlHits);<br />
                                                                //$rowAssCritUnitAssignment = $queryAssCritUnitAssignment-&gt;row();<br />
                                                                $num_rowHits = $queryHits-&gt;num_rows();</p>
                                                            <p>&nbsp;</p>
                                                            <p> if ($i % 2 == 0) {<br />
                                                                $BG = $GridRowLight;<br />
                                                                } else {<br />
                                                                $BG = $GridRowDark;<br />
                                                                }<br />
                                                            </p>
                                                            <p> $ID_String = $this-&gt;model_mysql_class-&gt;randStrGen(5);<br />
                                                                $ID_String_row = $this-&gt;model_mysql_class-&gt;randStrGen(5);</p>
                                                            <p>&nbsp;</p>
                                                            <p> echo '&lt;tr bgcolor=&quot;' . $BG . '&quot; &gt;';<br />
                                                                echo '&lt;td id=&quot;$ID_String&quot; colspan=&quot;7&quot;&gt;';</p>
                                                            <p> echo '&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; '<br />
                                                                . 'style=&quot;margin-top:20px; margin-bottom:5px;  border-bottom: 1px solid #F00;  background-color:#FFE1E2;  &quot;&gt;';<br />
                                                            </p>
                                                            <p> echo '&lt;td width=&quot;5%&quot;&gt;&lt;div align=&quot;left&quot;&gt;&lt;span style=&quot;font-family:Arial, '<br />
                                                                . 'Helvetica, sans-serif; font-size:12px; font-style:normal; '<br />
                                                                . 'font-weight:normal; margin-left:3px; &quot;&gt;' . $i . '&lt;/span&gt;&lt;/div&gt;&lt;/td&gt;';</p>
                                                            <p> echo '&lt;td width=&quot;30%&quot;&gt;&lt;div align=&quot;left&quot;&gt;&lt;span style=&quot;font-family:Arial, '<br />
                                                                . 'Helvetica, sans-serif; font-size:12px; font-style:normal; '<br />
                                                                . 'font-weight:normal; margin-left:3px; &quot; &gt;'<br />
                                                                . '&lt;a href=&quot;' . $UserURL . '&quot;&gt;'<br />
                                                                . '' . $this-&gt;model_mysql_class-&gt;StringTrim($UserURL, 30) . '&lt;/a&gt;'<br />
                                                                . '&lt;/span&gt;&lt;/div&gt;&lt;/td&gt;';</p>
                                                            <p> echo '&lt;td width=&quot;38%&quot;&gt;&lt;div align=&quot;left&quot;&gt;&lt;span style=&quot;font-family:Arial, '<br />
                                                                . 'Helvetica, sans-serif; font-size:12px; font-style:normal; '<br />
                                                                . 'font-weight:normal; margin-left:3px; &quot;&gt;'<br />
                                                                . '&lt;a href=&quot;' . base_url() . '?su=' . $shortner . '&quot;&gt;' . base_url() . '?su=' . $shortner . '&lt;/a&gt;'<br />
                                                                . '&lt;/span&gt;&lt;/div&gt;&lt;/td&gt;';<br />
                                                                echo '&lt;td width=&quot;10%&quot;&gt;&lt;div align=&quot;center&quot;&gt;&lt;span style=&quot;font-family:Arial, '<br />
                                                                . 'Helvetica, sans-serif; font-size:12px; font-style:normal; '<br />
                                                                . 'font-weight:normal; margin-left:3px;&quot;&gt;' . $num_rowHits . '&lt;/span&gt;&lt;/div&gt;&lt;/td&gt;';</p>
                                                            <p> echo '&lt;td width=&quot;12%&quot;&gt;&lt;div align=&quot;left&quot;&gt;&lt;span style=&quot;font-family:Arial, '<br />
                                                                . 'Helvetica, sans-serif; font-size:12px; font-style:normal; font-weight:normal; '<br />
                                                                . 'margin-left:3px; &quot;&gt;' . date('d m Y', $rec_time) . '&lt;/span&gt;&lt;/div&gt;&lt;/td&gt;';</p>
                                                            <p> echo '&lt;td width=&quot;6%&quot;&gt;'<br />
                                                                . '&lt;div align=&quot;center&quot; style=&quot;margin-top:3px;margin-bottom:3px; &quot; &gt;';<br />
                                                                echo '&lt;form name=&quot;form1&quot; id=&quot;test&quot; onSubmit=&quot;return false;&quot;  style=&quot;display: inline; margin: 0;&quot; &gt;';<br />
                                                                echo '&lt;input name=&quot;UserURLID&quot; type=&quot;hidden&quot; value=&quot;' . $UserURLID . '&quot;&gt;';<br />
                                                                echo '&lt;input name=&quot;ID_String&quot; type=&quot;hidden&quot; value=&quot;' . $ID_String_passed . '&quot;&gt;';<br />
                                                                echo '&lt;input type=&quot;image&quot; name=&quot;imageField&quot; id=&quot;imageField&quot; '<br />
                                                                . 'src=&quot;' . base_url() . 'imgs1/hide.png&quot; '<br />
                                                                . 'onclick=&quot;javascript: formget(this.form, \'' . base_url() . 'index.php/main/hide_user_url_register\', \'' . $ID_String_passed . '\');  &quot;  '<br />
                                                                . 'title=&quot;Hide&quot;  /&gt;';<br />
                                                                echo '&lt;/form&gt;';<br />
                                                                echo '&lt;/div&gt;&lt;/td&gt;';</p>
                                                            <p> echo '&lt;/tr&gt;';</p>
                                                            <p>&nbsp;</p>
                                                            <p> $i++;<br />
                                                                }</p>
                                                            <p>?&gt;</p>
                                                        </blockquote>
                                                    </li>
                                                    <li><span class="BulletBold" >Knowledge of database best practices</span>
                                                        <blockquote>
                                                            <p>For that type of service I think MyISAM storage engine, which is 256TB, takes precedens over InnoDB ACID Transactions. This demo is based on two MyISAM tables</p>
                                                            <p>-- Table structure for table `tblHits`<br />
                                                                --</p>
                                                            <p>CREATE TABLE IF NOT EXISTS `tblHits` (<br />
                                                                `HitID` bigint(20) NOT NULL AUTO_INCREMENT,<br />
                                                                `UserURLID_FK` bigint(20) NOT NULL,<br />
                                                                `HitDate` varchar(20) COLLATE utf8_unicode_ci NOT NULL,<br />
                                                                `HitIP` varchar(20) COLLATE utf8_unicode_ci NOT NULL,<br />
                                                                PRIMARY KEY (`HitID`)<br />
                                                                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;</p>
                                                            <p>-- --------------------------------------------------------</p>
                                                            <p>--<br />
                                                                -- Table structure for table `tblUserUrl`<br />
                                                                --</p>
                                                            <p>CREATE TABLE IF NOT EXISTS `tblUserUrl` (<br />
                                                                `UserURLID` bigint(20) NOT NULL AUTO_INCREMENT,<br />
                                                                `UserURL` varchar(256) COLLATE utf8_unicode_ci NOT NULL,<br />
                                                                `UserIP` varchar(20) COLLATE utf8_unicode_ci NOT NULL,<br />
                                                                `rec_time` varchar(30) COLLATE utf8_unicode_ci NOT NULL,<br />
                                                                `shortner` varchar(15) COLLATE utf8_unicode_ci NOT NULL,<br />
                                                                `visible` tinyint(1) NOT NULL DEFAULT '0',<br />
                                                                PRIMARY KEY (`UserURLID`)<br />
                                                                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;</p>
                                                            <p>&nbsp;</p>
                                                            <p>The table tblUserUrl holds users submitted long URLs and the table tblHits stores a row each time a user uses this system. That is intended for future analisys and statistics The tblHits is related to the tblUserUrl through foreign Key UserURLID_FK</p>
                                                        </blockquote>
                                                    </li>
                                                    <li><span class="BulletBold" >Ability to create simple but effective user interfaces (use of front-end frameworks are acceptable)</span>
                                                        <blockquote>
                                                            <p>The interface is very basic and is designed to provide a user to input a valid URL and display URL shortening result. Both long and short URL are clicable and function as requested</p>
                                                        </blockquote>
                                                    </li>
                                                    <li><span class="BulletBold" >Show us DRY, extensible development</span>
                                                        <blockquote>
                                                            <p>In this simple exercise any php code which repeats itself in more than one file is written in a separate file and than is included. In case of HTML whenever there is a large body of text There is only one tag which predifines a general font family, font-size and font-style lused Any exceptions uses CSS classes for desired output.</p>
                                                            <p>&nbsp;</p>
                                                            <p>Paul Sikora ..</p>
                                                        </blockquote>
                                                    </li>

                                                    <blockquote>
                                                        <p>&nbsp;</p>
                                                    </blockquote>
                                                </ul>
                                            </div></td>
                                    </tr>
                                </table>
                                <p>&nbsp;</p>
                            </div></td>
                    </tr>
                </table>
            </form>
        </div>

        <div id="ResultDiv" style="position:absolute; right:1%; top:20%; width:49%; 
             height:70%; z-index:1;">

            <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td id="URLShort" height="20%"  ><div align="center"><span style="background-color:#F5FEF5;  
                                                                               border-bottom:solid #DBEBD6 1px;  
                                                                               border-top:solid #DBEBD6 1px; font-family:Arial, Helvetica, sans-serif; font-size:24px; font-style:normal; font-weight:normal; margin-left:3px;">Latest URLs</span></div></td>
                </tr>

                <tr>
                    <td id="URLPage"  height="80%"  style="vertical-align:top;"  >

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>

                                <td width="5%">
                                    <div align="left">
                                        <span style="font-family:Arial, Helvetica, sans-serif; 
                                              font-size:14px; font-style:normal; 
                                              font-weight:normal; margin-left:3px; ">&nbsp;</span>
                                    </div>
                                </td>
                                <td width="30%">
                                    <div align="left">
                                        <span style="font-family:Arial, Helvetica, sans-serif; 
                                              font-size:14px; font-style:normal; 
                                              font-weight:normal; margin-left:3px; " >
                                            Original URL
                                        </span>
                                    </div>
                                </td>
                                <td width="38%">
                                    <div align="left">
                                        <span style="font-family:Arial, Helvetica, sans-serif; 
                                              font-size:14px; font-style:normal; 
                                              font-weight:normal; margin-left:3px; ">Short URL
                                        </span>
                                    </div>
                                </td>
                                <td width="15%">
                                    <div align="center">
                                        <span style="font-family:Arial, Helvetica, sans-serif; 
                                              font-size:14px; font-style:normal; 
                                              font-weight:normal; margin-left:3px;">Hits 
                                        </span>
                                    </div>
                                </td>
                                <td width="7%">
                                    <div align="left">
                                        <span style="font-family:Arial, Helvetica, sans-serif; 
                                              font-size:14px; font-style:normal; 
                                              font-weight:normal; margin-left:3px; ">Date</span>
                                    </div>
                                </td>
                                <td width="6%"><div align="center">&nbsp;</div></td>
                            </tr>      

                            <?php
                            $this->load->view('UserUrls/UserUrls');
                            ?>





                        </table>

                    </td>
                </tr>

            </table>
        </div>




    </body>
</html>